package main

import (
	"bitbucket.org/Iankill/svl-3d"
	"code.google.com/p/go-uuid/uuid"
	"fmt"
	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()
	router.GET("/", func(c *gin.Context) {
		c.String(200, "hello world")
	})

	router.GET("/test-solver", func(c *gin.Context) {
		newUUID := uuid.NewUUID()
		fmt.Println(newUUID.String())
		filePath := "/tmp/" + newUUID.String() + ".xml"
		ch := make(chan int)
		go svl.LaunchProgram("-shelf", 2000, 2000, filePath, ch)
		binHeight := <-ch
		fmt.Println(binHeight)
		b := svl.ParseXML(filePath, binHeight)
		c.JSON(200, b)
	})

	router.POST("/solve", func(c *gin.Context) {
		var json svl.Boost
		c.Bind(&json)
		fmt.Println(json.Max_height)
		newUUID := uuid.NewUUID()
		filePath := "/tmp/" + newUUID.String() + ".xml"
		svl.JsonToXMLproblem(json, filePath)
		ch := make(chan int)
		//Switch algorithm here
		switch json.Algorithm {
		case "shelf":
			go svl.LaunchProgram("-shelf", json.Base.Width, json.Base.Depth, filePath, ch)
		case "guillotine":
			go svl.LaunchProgram("-guillotine", json.Base.Width, json.Base.Depth, filePath, ch)
		case "global_guillotine":
			go svl.LaunchProgram("-global_guillotine", json.Base.Width, json.Base.Depth, filePath, ch)
		}
		binHeight := <-ch
		fmt.Println(binHeight)
		b := svl.ParseXML("output_file.xml", binHeight)
		c.JSON(200, b)
	})

	router.GET("/test-file", func(c *gin.Context) {
		filePath := "/Users/Triskay/Dev/Misc/3d-bin-packing/tests/test_10_3.xml"
		ch := make(chan int)
		go svl.LaunchProgram("-shelf", 200, 200, filePath, ch)
		binHeight := <-ch
		fmt.Println(binHeight)
		b := svl.ParseXML("output_file.xml", 100)
		c.JSON(200, b)

	})

	router.Run(":8888")
	fmt.Println("Test")

	fmt.Println("Salut")
}
